-- Erstelle die Datenbank, falls sie nicht existiert
CREATE DATABASE IF NOT EXISTS Produktbewertungen;

-- Verwende die erstellte Datenbank
USE Produktbewertungen;

-- Erstelle die Tabelle "Herkunft"
CREATE TABLE IF NOT EXISTS Herkunft (
    HerkunftID INT PRIMARY KEY,
    Herkunftsland VARCHAR(255),
    Hersteller VARCHAR(255)
);

-- Erstelle die Tabelle "Quellen"
CREATE TABLE IF NOT EXISTS Quellen (
    QuellenID INT PRIMARY KEY,
    Quellenname VARCHAR(255),
    Quellenart VARCHAR(255)
);

-- Erstelle die Tabelle "Produkte"
CREATE TABLE IF NOT EXISTS Produkte (
    ProduktID INT PRIMARY KEY,
    EAN VARCHAR(13) UNIQUE,
    Produktname VARCHAR(255),
    HerkunftID INT,
    AlternativProduktID INT,
    FOREIGN KEY (HerkunftID) REFERENCES Herkunft(HerkunftID),
    FOREIGN KEY (AlternativProduktID) REFERENCES Produkte(ProduktID)
);

-- Erstelle die Tabelle "Bewertungen"
CREATE TABLE IF NOT EXISTS Bewertungen (
    BewertungsID INT PRIMARY KEY,
    ProduktID INT,
    QuellenID INT,
    Bewertungspunkte DECIMAL(3, 1),
    Bewertungsdatum DATE,
    FOREIGN KEY (ProduktID) REFERENCES Produkte(ProduktID),
    FOREIGN KEY (QuellenID) REFERENCES Quellen(QuellenID)
);

-- Füge Beispieldaten in die Tabellen ein (optional)
INSERT INTO Herkunft (HerkunftID, Herkunftsland, Hersteller)
VALUES
    (1, 'USA', 'Hersteller A'),
    (2, 'Deutschland', 'Hersteller B');

INSERT INTO Quellen (QuellenID, Quellenname, Quellenart)
VALUES
    (1, 'NGO Bewertung', 'Benutzerbewertung'),
    (2, 'Expertenbewertung', 'Expertenbewertung');

INSERT INTO Produkte (ProduktID, EAN, Produktname, HerkunftID, AlternativProduktID)
VALUES
    (1, '1234567890', 'Produkt A', 1, 2),
    (2, '2345678901', 'Produkt B', 2, NULL),
    (3, '3456789012', 'Produkt C', 1, NULL);

INSERT INTO Bewertungen (BewertungsID, ProduktID, QuellenID, Bewertungspunkte, Bewertungsdatum)
VALUES
    (1, 1, 1, 4.5, '2023-02-05'),
    (2, 1, 2, 4.8, '2023-02-10'),
    (3, 2, 1, 4.0, '2023-01-25'),
    (4, 3, 2, 4.2, '2023-03-02');
