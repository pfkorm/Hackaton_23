

  const mysql = require('mysql');
  const express = require('express');
  const bodyParser = require('body-parser');
  const app = express();
  
  const con = mysql.createConnection({
    host: "78.47.49.188",
    user: "root",
    password: "password",
    port: 443, 
    database: "Produktbewertungen"
  });
  
  con.connect(function(err) {
    if (err) throw err;
    console.log("Mit der Datenbank verbunden");
  });
  
  app.listen(3000, () => {
    console.log("Anwendung gestartet und lauscht auf Port 3000");
  });
  
  app.use(express.static(__dirname));
  app.use(bodyParser.urlencoded({ extended: true }));
  
  app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
  });
  
  app.post("/", (req, res) => {
    var subName = req.body.test;
  
    // Datenbank Abfrage
    //con.query("SELECT * FROM Produktbewertungen.Bewertungen WHERE BewertungsID =?", [subName] , function (err, result, fields) {
      con.query("SELECT DISTINCT P.EAN, P.Produktname, H.Herkunftsland, H.Hersteller, A.Produktname AS AlternativProdukt, B.Bewertungspunkte, B.Bewertungsdatum FROM Produkte AS P LEFT JOIN Herkunft AS H ON P.HerkunftID = H.HerkunftID LEFT JOIN Produkte AS A ON P.AlternativProduktID = A.ProduktID LEFT JOIN Bewertungen AS B ON P.ProduktID = B.ProduktID WHERE P.EAN =?", [subName] , function (err, result, fields) {
      if (err) throw err;
      
 // HTML-Tabelle erstellen
 var htmlTable = "<table><tr><th>EAN</th><th>Product name</th><th>production country</th><th>manufacturer</th><th>Alternative product</th><th>reviews</th><th>Evaluation date</th></tr>";
 for (var i = 0; i < result.length; i++) {
   htmlTable += "<tr>";
   htmlTable += "<td>" + result[i].EAN + "</td>";
   htmlTable += "<td>" + result[i].Produktname + "</td>";
   htmlTable += "<td>" + result[i].Herkunftsland + "</td>";
   htmlTable += "<td>" + result[i].Hersteller + "</td>";
   htmlTable += "<td>" + result[i].AlternativProdukt + "</td>";
   htmlTable += "<td>" + getStars(result[i].Bewertungspunkte) + "</td>";
   htmlTable += "<td>" + result[i].Bewertungsdatum + "</td>";
   htmlTable += "</tr>";
 }
 htmlTable += "</table>";

 // Send data as html back
 res.send("HI, Here are the rating of your Product: " + subName + "<br>" + htmlTable);
});
});

// Funktion, um Sterne basierend auf den Bewertungspunkten zu erstellen
function getStars(rating) {
var stars = '';
for (var i = 0; i < rating; i++) {
 stars += '★'; // add stars 
}
return stars;
}
/*     // HTML-Tabelle erstellen
    var htmlTable = "<table><tr><th>EAN</th><th>Produktname</th><th>Herkunftsland</th><th>Hersteller</th><th>AlternativProdukt</th><th>Bewertungspunkte</th><th>Bewertungsdatum</th></tr>";
    for (var i = 0; i < result.length; i++) {
      htmlTable += "<tr>";
      htmlTable += "<td>" + result[i].EAN + "</td>";
      htmlTable += "<td>" + result[i].Produktname + "</td>";
      htmlTable += "<td>" + result[i].Herkunftsland + "</td>";
      htmlTable += "<td>" + result[i].Hersteller + "</td>";
      htmlTable += "<td>" + result[i].AlternativProdukt + "</td>";
      htmlTable += "<td>" + result[i].Bewertungspunkte + "</td>";
      htmlTable += "<td>" + result[i].Bewertungsdatum + "</td>";
      htmlTable += "</tr>";
    }
    htmlTable += "</table>";

    // Senden Sie die HTML-Tabelle als Antwort
    res.send("Hallo " + subName + "<br>" + htmlTable);
  });
}); */
   /*    console.log(result);
  
      res.send("Hallo " + subName + "<br>" + JSON.stringify(result));
    });
  });
   */

//  SELECT P.EAN, P.Produktname, H.Herkunftsland, H.Hersteller, A.Produktname AS AlternativProdukt, Q.Quellenname, B.Bewertungspunkte, B.Bewertungsdatum FROM Produkte AS P LEFT JOIN Herkunft AS H ON P.HerkunftID = H.HerkunftID LEFT JOIN Produkte AS A ON P.AlternativProduktID = A.ProduktID LEFT JOIN Bewertungen AS B ON P.ProduktID = B.ProduktID LEFT JOIN Quellen AS Q ON B.QuellenID = Q.QuellenID WHERE P.EAN = '1234567890';
