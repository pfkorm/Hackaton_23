# Hackaton 23 - Green Scoring

Green Scooring helps you while shopping to evaluate the sustainability of products based on different rating from different sources. Furthermore, we can also suggest you better alternative products. 
The software is opensource and just still in strong development.

## ⭐ Features

- Query of products with regard to their sustainability
- Sustainability score based on different sources
- Info material about sustainable products
- Current data

## 🔧 How to Install
🐳 Docker
docker run -d --restart=always -p 3001:3001 -v scorring:/app/data --name green-scooring
⚠️ Please use a local volume only. Other types such as NFS are not supported.

Green Scorring is now running local on http://localhost:3001

💪🏻 Non-Docker
Requirements:

Platform
✅ Major Linux distros such as Debian, Ubuntu, CentOS, Fedora and ArchLinux etc.
✅ Windows 10 (x64), Windows Server 2012 R2 (x64) or higher
❌ Replit / Heroku
Node.js 14 / 16 / 18 / 20.4
npm >= 7
Git
pm2 - For running Scoring in the background


## 🆕 What's Next/Roadmap?

- Add a barcode scanner
- More data sources
- Responsive design / better support for smartphones
- Be able to weight sustainability score

## Bug Reports / Feature Requests
If you want to report a bug or request a new feature, feel free to open a new issue.



| Test   | Test 2 | Test   |

| ------ | ------ | ------ |


